<?php
require 'vendor/autoload.php';
$parsedown = new ParsedownToC();
$parsedown->setBreaksEnabled(true);

$wrapClass = '';

$content = '## <center>not found</center>
**<center class="big-emoji">🫥</center>**';

$bRoot = ($_SERVER['REQUEST_URI'] == '/');

if ($bRoot)
  $content = '## <center>there is nothing to see here</center>
  <center class="big-emoji">🥷</center>';

$path = __DIR__.'/files'.urldecode($_SERVER['REQUEST_URI']);

// file was directly called instead of a nice URL
if (file_exists($path) && !$bRoot) {
  $mime = mime_content_type($path);

  // not a text based file -> output it's content directly
  if (
    strpos($mime, 'text/') === false &&
    $mime !== 'application/json'
  ) {
    $size = (string)(filesize($path));
    header('Content-type: '.$mime);
    header('Accept-Ranges: bytes');
    header('Content-Length: '.$size);
    header("Content-Disposition: inline;");
    header("Content-Range: bytes .$size");
    header("Content-Transfer-Encoding: binary\n");
    header('Connection: close');
    readfile($path);
    die();
  }
  
  $content = file_get_contents($path);

  if (
    $mime !== 'text/plain' &&
    $mime !== 'text/markdown'
  ) {
    if ($mime == 'application/json') {
      $content = json_encode(json_decode($content), JSON_PRETTY_PRINT);
    }

    // text based file, but no markdown -> echo as a code doocument
    $content = '<h3><center>'.basename($path).'</center></h3><pre>'.$content.'</pre>';
    $wrapClass = 'code';
  }
} else {
  // nice url with no file ending likely?
  // -> add suffix to check if a corresponding file exists
  if (!file_exists($path))
    $path = $path . '.md';

  if (!file_exists($path))
    $path = $path . '.markdown';

  if (!file_exists($path))
    $path = $path . '.txt';

  // no suffix needed or file found with one -> get escaped content
  if (file_exists($path) && !$bRoot)
    $content = '**Table of Content**

[toc]

<hr>

'.file_get_contents($path);
}

if (!file_exists($path) || $bRoot) {
  // still no luck? -> 404
  http_response_code(404);
}
?><html lang="en-US">
<head>
  <title><?php echo $_SERVER['REQUEST_URI']; ?></title>
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <link rel="stylesheet" href="/node_modules/github-markdown-css/github-markdown.css" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/11.9.0/styles/default.min.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/11.9.0/highlight.min.js"></script>
  <link rel="stylesheet" href="/css/style.css?v=<?php echo filemtime(__DIR__.'/css/style.css'); ?>" />
</head>
<body class="markdown-body">
  <div class="wrap <?php echo $wrapClass; ?>">
    <div class="logo-wrap">
      <div class="logo"></div>
    </div>
    <div class="content-wrap">
      <?php
      echo $parsedown->text($content);
      ?>
    </div>
  </div>
  <footer>
    <div class="wrap">
      <div class="contact">
        <p>
          <strong>Marco <small>L</small>a<small>UX</small></strong><br>
          design & development<br>
          <a target="_blank" href="https://laux.wtf">laux.wtf</a> / <a target="_blank" href="https://marcolaux.photo">marcolaux.photo</a>
        </p>
      </div>
      <div class="contact">
        <p>
          +49 1590 5434 192<br>
          marco@laux.wtf
        </p>
      </div>
    </div>
  </footer>
  <script>
    document.querySelectorAll('code, pre').forEach(el => {
      hljs.highlightElement(el);
    });
  </script>
</body>
