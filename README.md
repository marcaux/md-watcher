# md-watcher

### for information purposes

> copy markdown files (with images and files, relatively referenced) into the `files` folder
>
> the URL for a file in `files/markdown-file.md` will be `/markdown-file` and rendered in HTML via parsedown
> you can also reference files directly in the URL
>
> assets and files work as they should
> 
> it is therefore possible to just put Joplin or Anytype exports (or any markdown file with relative assets) into the files (or a sub-) folder and you have a nice URL to share with clients and others
>
> other text based documents get parsed and rendered as code blocks (like json or php files) if you just want to share code snippets

## requirements

- composer
- npm

## installation

- install required packages
```
composer install
npm install
```

## examples

### Markdown file

**via nice URL**

> filename: `files/get-started.md`

https://hostname/get-started

**via file reference**

![markdown render](https://gitlab.com/marcaux/md-watcher/-/raw/main/markdown.jpg "markdown render")

### PHP code snippet (directly called PHP file)

![code render](https://gitlab.com/marcaux/md-watcher/-/raw/main/php.jpg "code render")

## Apache configs

`/etc/apache2/sites-available/010-md-watcher.conf`

```
# Apache configuration file
# (e.g. /etc/apache2/sites-available/your-site.conf)

<VirtualHost *:80>
    ServerName your-hostname.com

    # DocumentRoot is the main web directory
    DocumentRoot /var/www/md-watcher

    # Allow any subdirectory to be used in the URL
    # This way sub directories like /article-xyz utilizes the /index.php
    <Directory "/var/www/md-watcher">
        Options FollowSymLinks

        AllowOverride All
        
        Require all granted
        
        RewriteEngine On
        RewriteRule .* - [E=HTTP_AUTHORIZATION:%{HTTP:Authorization}]
        RewriteBase /
        RewriteRule ^index\.php$ - [L]
        RewriteCond %{REQUEST_FILENAME} !-f
        RewriteCond %{REQUEST_FILENAME} !-d
        RewriteRule . /index.php [L]
    </Directory>

    # Stop rewriting of /files to make WebDAV work
    # Disabling DirectoryIndex so one needs to know the exact path and filename
    # Enable WebDAV so I can mount the files directory in our file manager
    <Directory "/var/www/md-watcher/files">
        RewriteEngine off

        DirectoryIndex disabled

        AuthType Basic
        AuthName "webdav"
        AuthUserFile /usr/local/apache2/md-watcher.htpasswd
        Require valid-user

        DAV On

        <IfModule mod_dav.c>
            DavDepthInfinity On
        </IfModule>
    </Directory>
</VirtualHost>

```
